package view;
import java.awt.Graphics;
import java.awt.Image;


public interface BackgroundHome {
	
	public void loadImage(Image img) ;
	
	public void paintComponent(Graphics g) ;

}
