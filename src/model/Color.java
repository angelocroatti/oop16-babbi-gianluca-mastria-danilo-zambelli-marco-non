package model;

import java.io.Serializable;

/**
 * Color is the enumeration for the color of players 
 *
 */
public enum Color implements Serializable {

	BLUE, ORANGE, YELLOW, RED, GREEN, PURPLE
}
